<?php

use App\Http\Controllers\SMS_MessageController;
use App\Http\Controllers\SuperController;
use App\Http\Controllers\SuperHeroController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::post('/login', [SuperHeroController::class, 'login']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [SuperController::class, 'index'])->name('home');
    Route::get('/users', [SuperController::class, 'getUsers'])->name('users');
    Route::get('/users/{id}', [SuperController::class, 'getUser'])->name('user');
    Route::get('/businesses', [SuperController::class, 'getBusinesses'])->name('businesses');
    Route::get('/shops', [SuperController::class, 'getShops'])->name('shops');
    Route::get('/managers', [SuperController::class, 'getManagers'])->name('managers');
    Route::get('/items', [SuperController::class, 'getItems'])->name('items');
    Route::get('/shop-items', [SuperController::class, 'getShopItems'])->name('shopItems');
    Route::get('/shop-services', [SuperController::class, 'getShopServices'])->name('shopServices');
    Route::resource('/admins', SuperHeroController::class);
    Route::post('send_sms', [SuperController::class, 'send_sms'])->name('send_sms');

    Route::resource('/messages', SMS_MessageController::class);


    Route::get('/bulk_messages', [SMS_MessageController::class, 'bulk_messages']);
    Route::post('/send_bulk_sms', [SMS_MessageController::class, 'send_bulk_sms'])->name('send_bulk_sms');
});
