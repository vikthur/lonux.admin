<?php

namespace App\Services;

use ErrorException;

class SendSMS {

    public function send_sms($message, $recipients)
    {
        $baseUrl = getenv("SENDCHAMP_BASE_URL");
        $apiUrl = "/sms/send";
        $token = getenv("SENDCHAMP_PUBLIC_KEY");
        $sender = getenv('SENDCHAMP_SENDER');
        $to = $recipients;
        $route = 'non_dnd';
        $text = $message;

        $post_body = [
            "sender_name" => $sender,
            "message" => $text,
            "route" => $route,
            "to" => $to,
        ];

        $post_body = json_encode($post_body);

        $curl = curl_init();

        curl_setopt_array($curl, [
        CURLOPT_URL => $baseUrl . $apiUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 120,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $post_body,
        CURLOPT_HTTPHEADER => [
            "Accept: application/json",
            "Authorization: Bearer ". $token,
            "Content-Type: application/json"
        ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $err = json_decode($err);
            throw new ErrorException($err->message);
        } else {
           return json_decode($response);
        }
    }

}