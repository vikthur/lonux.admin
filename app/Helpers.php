<?php

use Illuminate\Support\Facades\DB;

function checkIfUserHasBusiness($user)
{
    $business = DB::table('companies')->where('user_id', $user->id)->first();

    if (is_null($business)) {
        return "No";
    }

    return "Yes";
}

function checkIfUserHasShops($user)
{
    $shops = DB::table('shops')->where('user_id', $user->id)->get();

    if (count($shops) < 1) {
        return "No";
    }
    return "Yes (".count($shops) . ")";
}

function checkIfUserHasManager($shop)
{
    $managers = DB::table('shop_managers')->where('shop_key', $shop->key)->get();

    if (count($managers) < 1) {
        return "No";
    }
    return "Yes (".count($managers) . ")";
}
