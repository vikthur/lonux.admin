<?php

namespace App\Console\Commands;

use App\Models\SuperHero;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class InitApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize the admin dashboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function passwordInput()
    {
        $password = $this->secret('Choose password');
        $password2 = $this->secret('Confirm password');

        if ($password != $password2)
        {
            $this->error('Passwords do not match, try again');

            $this->passwordInput();
        }

        return $password;
    }

    public function createAdmin($name, $phone, $password)
    {
        $superHero = new SuperHero();

        $check = SuperHero::where('phone', $phone)->first();
        if (!is_null($check))
        {
            throw new Exception("Phone Number already exixts", 1);
            
        }

        $superHero->name = $name;
        $superHero->phone = $phone;

        $superHero->password = Hash::make($password);
        $superHero->is_active = true;
        $superHero->level = 0;
        $superHero->email = $this->uniqueEmail($name);
        $superHero->last_login = now();

        $superHero->save();

        return "Admin Created Successfully";
    }

    public function uniqueEmail($name)
    {
        $email = str_replace(' ', '', ucwords($name));
        $check = SuperHero::whereEmail($email)->first();
        if (!is_null($check)) {
            $email = $email . time();
        }

        return $email;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('======= Setting up '. env('APP_NAME') . ' ========');

        $name = $this->ask('Admin Name');
        $phone = $this->ask('Admin Phone Number');
        $password = $this->passwordInput();

        try {
            $createUser = $this->createAdmin($name, $phone, $password);
            $this->info($createUser);
        } catch (\Throwable $th) {
            $this->error($th);
        }

        return 0;
    }
}
