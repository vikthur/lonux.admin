<?php

namespace App\Jobs;

use App\Services\SendSMS;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendBulkSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $users = [];
    public $message = '';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users, $message)
    {
        $this->users = $users;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sms_service = new SendSMS();
        try {
            $sms_service->send_sms($this->message, $this->users);
        } catch (\Throwable $th) {
            report($th);
        }
    }
}
