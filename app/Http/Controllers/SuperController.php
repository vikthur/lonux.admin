<?php

namespace App\Http\Controllers;

use App\Models\SMS_Message;
use App\Services\SendSMS;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function PHPUnit\Framework\isNull;

class SuperController extends Controller
{
    public function index()
    {
        $arr = ['users', 'companies', 'shops', 'shop_managers', 'items', 'shop_items', 'offered_services', 'super_heroes'];
        $lonuxStats = $this->runQueries($arr);

        return view('welcome', compact('lonuxStats'));
    }

    public function getUsers()
    {
        $users = DB::table('users')
            ->join('account_types', 'users.account_type_id', '=', 'account_types.id')
            ->select('users.*', 'account_types.name as account_type')->orderBy('created_at', 'desc')->get();
        return view('users.index', compact('users'));
    }

    public function getBusinesses()
    {
        $businesses = DB::table('companies')->get();

        return view('businesses.index', compact('businesses'));
    }

    public function getShops()
    {
        $shops = DB::table('shops')
            ->join('shop_services', 'shops.shop_service_id', '=', 'shop_services.id')
            ->select('shops.*', 'shop_services.name as shop_type')->get();

        return view('businesses.shops.index', compact('shops'));
    }

    public function getManagers()
    {
        $managers = DB::table('shop_managers')
            ->join('shops', 'shop_managers.shop_key', '=', 'shops.key')
            ->select('shop_managers.*', 'shops.shop_name as shop_name')->get();

        return view('businesses.shops.managers', compact('managers'));
    }

    public function getItems()
    {
        $items = DB::table('items')
            ->join('item_categories', 'items.item_category_id', '=', 'item_categories.id')
            ->select('items.*', 'item_categories.item as category')->get();

        return view('items.index', compact('items'));
    }

    public function getShopItems()
    {
        $shopItems = DB::table('shop_items')
            ->join('shops', 'shop_items.shop_id', '=', 'shops.id')
            ->select('shop_items.*', 'shops.shop_name')->get();

        return view('businesses.shops.items', compact('shopItems'));
    }

    public function getShopServices()
    {
        $shopServices = DB::table('offered_services')
            ->join('shops', 'offered_services.shop_key', '=', 'shops.key')
            ->select('offered_services.*', 'shops.shop_name')->get();

        return view('businesses.shops.services', compact('shopServices'));
    }

    public function getUser($id)
    {
        $user = DB::table('users')
            ->join('account_types', 'users.account_type_id', '=', 'account_types.id')
            ->select('users.*', 'account_types.name as account_type')->where('users.id', $id)->first();

        $business = DB::table('companies')->where('user_id', $id)->first();
        $shops = DB::table('shops')->where('user_id', $id)->get();
        $messages = SMS_Message::all();

        if (!is_null($business)) $user->business = $business;

        if (count($shops)) $user->shops = $shops;

        return view('users.show', compact('user', 'messages'));
    }

    public function send_sms(Request $request)
    {
        $text = substr($request->sms_text, 0, 160);

        if ($text == "")
        {
            $id = $request->sms_select;
            $message = SMS_Message::find($id);
            $text = $message->message;
        }
        $recipient = [$request->recipient];

        $sms_service = new SendSMS();

        try {
            $send = $sms_service->send_sms($text, $recipient);
            $request->session()->flash('success', $send->message);
        } catch (\Throwable $th) {
            $request->session()->flash('error', $th->getMessage());
        }

        return back();
    }

    public function runQueries($tables = [])
    {
        $results = [];
        foreach ($tables as $key => $table) {
            $count = count(DB::select('select * from ' . $table));
            $results[$table] = $count;
        }

        return $results;
    }
}
