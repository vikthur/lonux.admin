<?php

namespace App\Http\Controllers;

use App\Models\SuperHero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SuperHeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $heroes = SuperHero::paginate(10);

        return view('admins.index', compact('heroes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|regex:/(0)[0-9]{10}/|unique:super_heroes'
        ]);

        $password = "password";
        $superHero = new SuperHero();

        $superHero->name = $request->name;
        $superHero->phone = $request->phone;

        $superHero->password = Hash::make($password);
        $superHero->is_active = true;
        $superHero->level = 0;
        $superHero->email = $this->uniqueEmail($request->name);

        $superHero->save();

        session()->flash('success', "Operation Successful");
        session()->flash('user', $superHero);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uniqueEmail($name)
    {
        $email = str_replace(' ', '', ucwords($name));
        $check = SuperHero::whereEmail($email)->first();
        if (!is_null($check)) {
            $email = $email . time();
        }

        return $email;
    }

    public function login(Request $request)
    {
        $phone = $request->phone;
        $pass = $request->password;

        $hero = Auth::attempt([
            'phone' => $phone,
            'password' => $pass
        ]);

        if ($hero) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        } else {
            session()->flash('unauthorized', 'Unauthorized !!');
            return redirect()->back();
        }
    }
}
