<?php

namespace App\Http\Controllers;

use App\Jobs\SendBulkSMS;
use App\Models\SMS_Message;
use App\Services\SendSMS;
use ErrorException;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client;

class SMS_MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = SMS_Message::all();

        return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'message' => 'required|string|max:160'
        ]);

        $message = new SMS_Message();

        $message->title = Str::slug($request->title .' '. time());
        $message->message = $request->message;
        
        $message->save();

        $request->session()->flash('success', "Message stored successfully");

        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = SMS_Message::findorfail($id);

        return view('messages.edit', compact('message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'message' => 'required|string|max:160'
        ]);
        $message = SMS_Message::findorfail($id);

        $message->message = $request->message;

        $message->save();

        $request->session()->flash('success', "Message stored successfully");

        return back();
    }

    public function bulk_messages()
    {
        $messages = SMS_Message::all();

        return view('messages.bulk', compact('messages'));
    }

    public function send_bulk_sms(Request $request)
    {
        $this->validate($request, [
            'sms_select' => 'required',
            'sms_recipients' => 'required'
        ]);

        $message = SMS_Message::findorfail($request->sms_select);
        $message = $message->message;
        $users = $this->get_sms_users($request->sms_recipients);

        $length = count($users);

        for ($i=0; $i < $length; $i++) { 
            $sub_arr = $users[$i];
            $new_users_sub_arr = [];
            foreach ($sub_arr as $key => $arr) {
                $phone = '+'.$arr->phone;
                array_push($new_users_sub_arr, $phone);
            }

            $users[$i] = $new_users_sub_arr;

            SendBulkSMS::dispatch($users[$i], $message)->delay(now()->addMinutes(1));
        }
        
        request()->session()->flash("success", "SMS queued for processing");
        return back();

    }

    public function formatContacts(Array $array)
    {
        if (count($array) > 50)
        {
            $distructuredArray = [];
            $loopLength = count($array) / 50;

            for ($i=0; $i < $loopLength; $i++) { 
                $new_arr = array_splice($array, 0, 50);

                $distructuredArray[$i] = $new_arr;
            }

            return $distructuredArray;
        }

        return $array;
    }

    public function get_sms_users($selector)
    {
        switch ($selector) {
            case 'all_business_users':
                $users = DB::table('companies')
                ->join('users', 'companies.user_id', '=', 'users.id')
                ->select('users.phone as phone')->get()->toArray();
                return $this->formatContacts($users);
                break;

            case 'business_without_shops':
                $users = DB::table('companies')
                ->join('users', 'companies.user_id', '=', 'users.id')
                ->select('users.phone as phone', 'users.id as id')->get()->toArray();

                $business_without_shops = [];
                foreach ($users as $key => $user) {
                    $check = checkIfUserHasShops($user);

                    if($check == "No")
                    {
                        array_push($business_without_shops, $user);
                    }
                }
                return $this->formatContacts($business_without_shops);
                break;

            case 'shops_without_managers':
                $shops = DB::table('shops')->get();
                $shops_without_managers = [];
                foreach ($shops as $key => $shop) {
                    $check = checkIfUserHasManager($shop);
                    if($check == "No")
                    {
                        $user = DB::table('users')->whereId($shop->user_id)->select('phone')->first();
                        array_push($shops_without_managers, $user);
                    }
                }
                return $this->formatContacts($shops_without_managers);
                break;

            case 'shop_managers':
                $users = DB::table('shop_managers')->get()->toArray();
                return $this->formatContacts($users);
                break;
            
            default:
                return [];
                break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = SMS_Message::findorfail($id);

        $message->delete();

        request()->session()->flash('done', "message deleted successfully");

        return back();
    }
}
