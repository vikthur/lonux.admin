<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.title-meta', ['title' => 'Lonux Users'])

    <!-- third party css -->
    <link href="{{ asset('new_look/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('new_look/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('new_look/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css') }}"
        rel="stylesheet" type="text/css" />
    <link href="{{ asset('new_look/assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css') }}"
        rel="stylesheet" type="text/css" />
    <!-- third party css end -->

    @include('partials.head-css')

</head>

<body class="loading">

    <!-- Begin page -->
    <div id="wrapper">

        @include('partials.left-sidebar')

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">

            @include('partials/topbar', [
                'title' => 'Lonux Users',
                'subtitle' => 'Lists',
                'item' => 'Tables',
                'activeitem' => 'Users',
            ])

            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">

                                    <h4 class="header-title mb-3">total users on lonux</h4>

                                    <table id="datatable-buttons"
                                        class="table table-bordered table-striped dt-responsive nowrap w-100">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Registered</th>
                                                <th>Account Type</th>
                                                <th>Has Business</th>
                                                <th>Has Shop(s)</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            @foreach ($users as $key => $user)
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->phone }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($user->created_at)->diffForHumans() }}
                                                    <td>{{ $user->account_type }}</td>
                                                    <td>{{ checkIfUserHasBusiness($user) }}</td>
                                                    <td>{{ checkIfUserHasShops($user) }}</td>
                                                    <td><a href="{{ route('user', $user->id) }}">View</a></td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                    </div>
                    <!-- end row-->

                </div> <!-- container-fluid -->

            </div> <!-- content -->

            @include('partials.footer')

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    @include('partials.right-sidebar')

    <!-- Vendor js -->
    <script src="{{ asset('new_look/assets/js/vendor.min.js') }}"></script>

    <!-- third party js -->
    <script src="{{ asset('new_look/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Buttons js -->
    <script src="{{ asset('new_look/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('new_look/assets/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/datatables.net-select/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/jszip/jszip.min.js') }}"></script>

    <!-- Responsive js -->
    <script src="{{ asset('new_look/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}">
    </script>

    <!-- Datatables init -->
    <script src="{{ asset('new_look/assets/js/pages/datatables.init.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('new_look/assets/js/app.min.js') }}"></script>

</body>

</html>
