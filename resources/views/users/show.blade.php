@extends('layouts.app')


@section('title')
    @include('partials.title-meta', ['title' => 'Lonux Users'])
@endsection

@section('topbar')
    @include('partials.topbar', [
        'title' => 'Lonux User',
        'subtitle' => 'Lists',
        'item' => 'Tables',
        'activeitem' => 'User',
    ])
@endsection



@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- meta -->
            <div class="profile-user-box card bg-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="float-left mr-2"><img src="{{ asset('new_look/assets/images/users/user.png') }}"
                                    alt="" class="avatar-xl rounded-circle"></span>
                            <div class="media-body text-white">
                                <h4 class="mt-1 mb-1 text-white font-18">{{ $user->name }}</h4>
                                <p class="font-13"> {{ $user->account_type }}</p>
                                <p class="mb-0">{{ $user->address }}</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-right">
                                <button type="button" class="btn btn-light waves-effect">
                                    <i class="mdi mdi-account-settings mr-1"></i> Edit Profile
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ meta -->
        </div>
    </div>
    <!-- end row -->


    <div class="row">
        <div class="col-xl-4">
            <!-- Personal-Information -->
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mb-3">User's Information</h4>
                    <div class="panel-body">
                        <hr />

                        <div class="text-left text-muted font-13">
                            <p class="mb-2"><strong>Full Name :</strong> <span>{{ $user->name }}</span></p>

                            <p class="mb-2"><strong>Mobile :</strong><span>{{ $user->phone }}</span></p>

                            <p class="mb-2"><strong>Email :</strong>
                                <span>{{ $user->email }}</span>
                            </p>

                            <p class="mb-2"><strong>Location :</strong> <span>{{ $user->address }}</span></p>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Personal-Information -->

            <div class="card ribbon-box">
                <div class="card-body">
                    <div class="ribbon ribbon-blue float-left">Messages</div>
                    <div class="clearfix"></div>
                    <div class="inbox-widget" data-simplebar style="max-height: 350px;">
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-2.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Tomaslau</p>
                            <p class="inbox-item-text">I've finished it! See you so...</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-3.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Stillnotdavid</p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-4.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Kurafire</p>
                            <p class="inbox-item-text">Nice to meet you</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-5.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Shahedk</p>
                            <p class="inbox-item-text">Hey! there I'm available...</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-6.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Adhamdannaway</p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>

                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-3.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Stillnotdavid</p>
                            <p class="inbox-item-text">This theme is awesome!</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>
                        <div class="inbox-item">
                            <div class="inbox-item-img"><img src="../assets/images/users/avatar-4.jpg"
                                    class="rounded-circle" alt=""></div>
                            <p class="inbox-item-author">Kurafire</p>
                            <p class="inbox-item-text">Nice to meet you</p>
                            <p class="inbox-item-date">
                                <a href="javascript:(0);" class="btn btn-sm btn-success waves-effect waves-light"> Reply
                                </a>
                            </p>
                        </div>
                    </div> <!-- end inbox-widget -->
                </div>
            </div>

        </div>


        <div class="col-xl-8">

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="card tilebox-one">
                        <div class="card-body">
                            <i class="icon-layers float-right text-muted h2 m-0"></i>
                            <h6 class="text-muted text-uppercase mt-0">Orders</h6>
                            <h2><span data-plugin="counterup">1,587</span></h2>
                            <div class="text-truncate">
                                <span class="badge badge-primary"> +11% </span> <span class="text-muted">From previous
                                    period</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="card tilebox-one">
                        <div class="card-body">
                            <i class="icon-paypal float-right text-muted h2 m-0"></i>
                            <h6 class="text-muted text-uppercase mt-0">Revenue</h6>
                            <h2>$<span data-plugin="counterup">46,782</span></h2>
                            <div class="text-truncate">
                                <span class="badge badge-danger"> -29% </span> <span class="text-muted">From previous
                                    period</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="card tilebox-one">
                        <div class="card-body">
                            <i class="icon-rocket float-right text-muted h2 m-0"></i>
                            <h6 class="text-muted text-uppercase mt-0">Product Sold</h6>
                            <h2><span data-plugin="counterup">1,890</span></h2>
                            <div class="text-truncate">
                                <span class="badge badge-primary"> +89% </span> <span class="text-muted">Last
                                    year</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mb-3">Business Details</h4>
                    @if (isset($user->business))
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>Business Name</th>
                                        <th>Business Email</th>
                                        <th>Business Address</th>
                                        <th>NAFDAC Number</th>
                                        <th>CAC Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $user->business->name }}</td>
                                        <td>{{ $user->business->email }}</td>
                                        <td>{{ $user->business->hq_address }}</td>
                                        <td>{{ $user->business->nafdac_reg_no }}</td>
                                        <td>{{ $user->business->cac_reg_no }}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-warning">No Business</div>
                    @endif

                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mb-3">Business' Shops</h4>
                    @if (isset($user->shops))
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Shop Name</th>
                                        <th>Shop Address</th>
                                        <th>Open At</th>
                                        <th>Close At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user->shops as $key => $shop)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{ $shop->shop_name }}</td>
                                            <td>{{ $shop->shop_address }}</td>
                                            <td>{{ $shop->shop_open_at }}</td>
                                            <td>{{ $shop->shop_close_at }}</td>
                                            <td>View</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-warning">No Shops</div>
                    @endif
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mb-3">Send text message</h4>
                    @if (Session::has('success'))
                        <div class="alert alert-success">{{ session('success') }}</div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif
                    <form class="form" method="post" action="{{ route('send_sms') }}">
                        @csrf
                        <textarea name="sms_text" class="form-control" rows="5" id="sms_text"></textarea>
                        <input type="hidden" name="recipient" value="{{ $user->phone }}" />
                        <p class="text-right mt-2 lead"><span id="writtenText">0</span>/160</p>
                        <p class="text-center">OR</p>
                        <h4 class="header-title mb-3">Choose saved texts</h4>
                        <select name="sms_select" class="form-control mb-3">
                            @foreach ($messages as $message)
                                <option value="{{$message->id}}">{{$message->title}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-primary waves-light waves-effect">Send SMS</button>
                    </form>
                </div>
            </div>



        </div>
        <!-- end col -->

    </div>
    <!-- end row -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('textarea#sms_text').keyup(function() {
                const writtenTextSpan = $('span#writtenText')
                const tis = $(this)
                const val = tis.val().substr(0, 160)
                const length = val.length
                writtenTextSpan.text(length)
                tis.val(val)

            })
        })
    </script>
@endsection
