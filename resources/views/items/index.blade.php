@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "Inventory Items" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Inventory"])
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">lonux inventory items</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Item Cost</th>
                                <th>Item Category</th>
                                <th>UPC</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($items as $key => $item)
                                <tr>
                                    <td>{{ $item->item_name }}</td>
                                    <td>{{ $item->item_cost }}</td>
                                    <td>{{ $item->category }}</td>
                                    <td>{{ $item->upc }}</td>
                                    <td><a href="/items/{{ $item->id }}">View</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
