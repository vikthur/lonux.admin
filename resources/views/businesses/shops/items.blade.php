@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "Shop Items" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux Shops","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Inventory"])
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">lonux shop items</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Item Cost</th>
                                <th>Sold</th>
                                <th>Shop</th>
                                <th>UPC</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($shopItems as $key => $shopItem)
                                <tr>
                                    <td>{{ $shopItem->name }}</td>
                                    <td>{{ $shopItem->amount }}</td>
                                    <td>{{ $shopItem->sold }}</td>
                                    <td>{{ $shopItem->shop_name }}</td>
                                    <td>{{ $shopItem->upc }}</td>
                                    <td><a href="/businesses/shopItems/{{ $shopItem->id }}">View</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
