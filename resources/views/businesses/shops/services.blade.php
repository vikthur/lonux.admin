@extends('layouts.app')


@section('title')
    @include("partials.title-meta", ["title" => "Shop Services" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux Shops","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Services"])
@endsection



@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">services rendered by shops on lonux</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Service Name</th>
                                <th>Service Duration</th>
                                <th>Service Charge</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($shopServices as $key => $shopService)
                                <tr>
                                    <td>{{ $shopService->service_name }}</td>
                                    <td>{{ $shopService->service_duration }}</td>
                                    <td>{{ $shopService->service_charge }}</td>
                                    <td><a href="/businesses/shopServicess/{{ $shopService->id }}">View</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
