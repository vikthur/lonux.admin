@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "Shops" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux Shops","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Shops"])
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">shops on lonux</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Shop Name</th>
                                <th>Shop Type</th>
                                <th>Shop Address</th>
                                <th>Contact Code</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($shops as $key => $shop)
                                <tr>
                                    <td>{{ $shop->shop_name }}</td>
                                    <td>{{ $shop->shop_type }}</td>
                                    <td>{{ $shop->shop_address }}</td>
                                    <td>{{ $shop->contact_code }}</td>
                                    <td><a href="/businesses/shops/{{ $shop->key }}">View</a></td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
