@extends('layouts.app')


@section('title')
    @include("partials.title-meta", ["title" => "Shop Managers" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux Businesses","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Shop Managers"])
@endsection



@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">shop managers on lonux</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Shop Name</th>
                                <th>Phone</th>
                                <th>Is Active</th>
                                <th>Last Login</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($managers as $key => $manager)
                                <tr>
                                    <td>{{ $manager->name }}</td>
                                    <td>{{ $manager->shop_name }}</td>
                                    <td>{{ $manager->phone }}</td>
                                    <td>{{ $manager->is_active == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ $manager->last_login ? Carbon\Carbon::parse($manager->last_login)->diffForHumans() : 'Not yet' }}
                                    </td>
                                    <td><a href="/managers/{{ $manager->id }}">View</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
