@extends('layouts.app', ['title' => "Businesses"])


@section('title')
    @include("partials.title-meta", ["title" => "Businesses" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux Businesses","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Businesses"])
@endsection



@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">user's businesse on lonux</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>CAC</th>
                                <th>NAFDAC</th>
                                <th>Action</th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach ($businesses as $key => $business)
                                <tr>
                                    <td>{{ $business->name }}</td>
                                    <td>{{ $business->email }}</td>
                                    <td>{{ $business->hq_address }}</td>
                                    <td>{{ $business->cac_reg_no }}</td>
                                    <td>{{ $business->nafdac_reg_no }}</td>
                                    <td><a href="/businesses/{{ $business->id }}">View</a></td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
