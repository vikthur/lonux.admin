<!DOCTYPE html>
<html lang="en">

<head>
    @include("partials.title-meta", ["title" => "Dashboard"])

    @include('partials.head-css')

</head>

<body class="loading">

    <!-- Begin page -->
    <div id="wrapper">

        @include('partials.left-sidebar')

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">

            @include('partials.topbar', ['title' => 'Dashboard', 'subtitle' => 'Home', 'item' => 'Dashboard',
            'activeitem' => 'Overview'])

            <div class="content">

                <!-- Start Content-->
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('users') }}" class="media align-items-center">
                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Users</p>
                                            <h3 class="mb-0">{{ $lonuxStats['users'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#0acf97" value="37" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('businesses') }}" class="media align-items-center">
                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Businesses</p>
                                            <h3 class="mb-0">{{ $lonuxStats['companies'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#f9bc0b" value="92" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('shops') }}" class="media align-items-center">
                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Shops</p>
                                            <h3 class="mb-0">{{ $lonuxStats['shops'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#f1556c" value="14" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('managers') }}" class="media align-items-center">

                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Shop Managers</p>
                                            <h3 class="mb-0">{{ $lonuxStats['shop_managers'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#2d7bf4" value="60" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('items') }}" class="media align-items-center">

                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Items</p>
                                            <h3 class="mb-0">{{ $lonuxStats['items'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#f1556c" value="60" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('shopItems') }}" class="media align-items-center">

                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Shop Items</p>
                                            <h3 class="mb-0">{{ $lonuxStats['shop_items'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#2d7bf4" value="60" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('shopServices') }}" class="media align-items-center">

                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Shop Services</p>
                                            <h3 class="mb-0">{{ $lonuxStats['offered_services'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#f9bc0b" value="60" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{ route('admins.index') }}" class="media align-items-center">

                                        <div class="widget-chart-two-content media-body">
                                            <p class="text-muted mb-0">Super Heroes</p>
                                            <h3 class="mb-0">{{ $lonuxStats['super_heroes'] }}</h3>
                                        </div>
                                        <div dir="ltr" class="knob-chart">
                                            <input data-plugin="knob" data-width="80" data-height="80"
                                                data-linecap=round data-bgColor="rgba(150, 150, 150, 0.1)"
                                                data-fgColor="#0acf97" value="100" data-skin="tron"
                                                data-angleOffset="180" data-readOnly=true data-thickness=".1" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end row -->



                    <div class="row">
                        <div class="col-xl-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title">Order Overview</h4>

                                    <div id="website-stats" style="height: 350px;" class="flot-chart mt-4"
                                        data-colors="#02c0ce,#2d7bf4,#f1556c"></div>

                                </div>
                            </div>
                        </div>

                        <div class="col-xl-6">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title">Sales Overview</h4>

                                    <div id="combine-chart" data-colors="#e3eaef,#f1556c,#02c0ce"
                                        class="flot-chart mt-4" style="height: 350px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->


                    <div class="row">
                        <div class="col-xl-8">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title mb-3">Wallet Balances</h4>

                                    <div class="table-responsive">
                                        <table class="table table-hover table-centered table-nowrap m-0">

                                            <thead>
                                                <tr>
                                                    <th>Profile</th>
                                                    <th>Name</th>
                                                    <th>Currency</th>
                                                    <th>Balance</th>
                                                    <th>Reserved in orders</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <img src="../assets/images/users/avatar-2.jpg" alt="contact-img"
                                                            title="contact-img" class="rounded-circle avatar-sm" />
                                                    </td>

                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Tomaslau</h5>
                                                        <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                                    </td>

                                                    <td>
                                                        <i class="mdi mdi-currency-btc text-primary"></i> BTC
                                                    </td>

                                                    <td>
                                                        0.00816117 BTC
                                                    </td>

                                                    <td>
                                                        0.00097036 BTC
                                                    </td>

                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-primary"><i
                                                                class="mdi mdi-plus"></i></a>
                                                        <a href="#" class="btn btn-sm btn-danger"><i
                                                                class="mdi mdi-minus"></i></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <img src="../assets/images/users/avatar-3.jpg" alt="contact-img"
                                                            title="contact-img" class="rounded-circle avatar-sm" />
                                                    </td>

                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Erwin E. Brown</h5>
                                                        <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                                    </td>

                                                    <td>
                                                        <i class="mdi mdi-currency-eth text-primary"></i> ETH
                                                    </td>

                                                    <td>
                                                        3.16117008 ETH
                                                    </td>

                                                    <td>
                                                        1.70360009 ETH
                                                    </td>

                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-primary"><i
                                                                class="mdi mdi-plus"></i></a>
                                                        <a href="#" class="btn btn-sm btn-danger"><i
                                                                class="mdi mdi-minus"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="../assets/images/users/avatar-4.jpg" alt="contact-img"
                                                            title="contact-img" class="rounded-circle avatar-sm" />
                                                    </td>

                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Margeret V. Ligon</h5>
                                                        <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                                    </td>

                                                    <td>
                                                        <i class="mdi mdi-currency-eur text-primary"></i> EUR
                                                    </td>

                                                    <td>
                                                        25.08 EUR
                                                    </td>

                                                    <td>
                                                        12.58 EUR
                                                    </td>

                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-primary"><i
                                                                class="mdi mdi-plus"></i></a>
                                                        <a href="#" class="btn btn-sm btn-danger"><i
                                                                class="mdi mdi-minus"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="../assets/images/users/avatar-5.jpg" alt="contact-img"
                                                            title="contact-img" class="rounded-circle avatar-sm" />
                                                    </td>

                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Jose D. Delacruz</h5>
                                                        <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                                    </td>

                                                    <td>
                                                        <i class="mdi mdi-currency-cny text-primary"></i> CNY
                                                    </td>

                                                    <td>
                                                        82.00 CNY
                                                    </td>

                                                    <td>
                                                        30.83 CNY
                                                    </td>

                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-primary"><i
                                                                class="mdi mdi-plus"></i></a>
                                                        <a href="#" class="btn btn-sm btn-danger"><i
                                                                class="mdi mdi-minus"></i></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <img src="../assets/images/users/avatar-6.jpg" alt="contact-img"
                                                            title="contact-img" class="rounded-circle avatar-sm" />
                                                    </td>

                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Luke J. Sain</h5>
                                                        <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                                    </td>

                                                    <td>
                                                        <i class="mdi mdi-currency-btc text-primary"></i> BTC
                                                    </td>

                                                    <td>
                                                        2.00816117 BTC
                                                    </td>

                                                    <td>
                                                        1.00097036 BTC
                                                    </td>

                                                    <td>
                                                        <a href="#" class="btn btn-sm btn-primary"><i
                                                                class="mdi mdi-plus"></i></a>
                                                        <a href="#" class="btn btn-sm btn-danger"><i
                                                                class="mdi mdi-minus"></i></a>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-xl-4">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="header-title">Total Wallet Balance</h4>


                                    <div id="donut-chart" class="flot-chart mt-5"
                                        data-colors="#02c0ce,#2d7bf4,#e3eaef,#f1556c,#f9bc0b" style="height: 340px;">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end row -->

                </div> <!-- container-fluid -->

            </div> <!-- content -->

            @include('partials.footer')

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->

    @include('partials.right-sidebar')

    <!-- Vendor js -->
    <script src="{{ asset('new_look/assets/js/vendor.min.js') }}"></script>

    <!-- flot-charts js -->
    <script src="{{ asset('new_look/assets/libs/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/flot-charts/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/flot-charts/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/flot-charts/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/flot-charts/jquery.flot.crosshair.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/flot.curvedlines/curvedLines.js') }}"></script>
    <script src="{{ asset('new_look/assets/libs/flot-axislabels/jquery.flot.axislabels.js') }}"></script>

    <!-- KNOB JS -->
    <script src="{{ asset('new_look/assets/libs/jquery-knob/jquery.knob.min.js') }}"></script>

    <!-- Dashboard init js-->
    <script src="{{ asset('new_look/assets/js/pages/dashboard.init.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('new_look/assets/js/app.min.js') }}"></script>

</body>

</html>
