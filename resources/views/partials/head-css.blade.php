<!-- App css -->
<link href="{{ asset('new_look/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"
    id="bs-default-stylesheet" />
<link href="{{ asset('new_look/assets/css/app.min.css') }}" rel="stylesheet" type="text/css"
    id="app-default-stylesheet" />

<link href="{{ asset('new_look/assets/css/bootstrap-dark.min.css') }}" rel="stylesheet" type="text/css"
    id="bs-dark-stylesheet" />
<link href="{{ asset('new_look/assets/css/app-dark.min.css') }}" rel="stylesheet" type="text/css"
    id="app-dark-stylesheet" />

<!-- icons -->
<link href="{{ asset('new_look/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
