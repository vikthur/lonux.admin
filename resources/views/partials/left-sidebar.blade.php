<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <!-- LOGO -->
    <div class="logo-box">
        <a href="{{ route('home') }}" class="logo logo-dark text-center">
            <span class="logo-sm">
                <img src="{{ asset('new_look/assets/images/lonux-logo-300.png') }}" alt="" height="24">
                <!-- <span class="logo-lg-text-light">Highdmin</span> -->
            </span>
            <span class="logo-lg">
                <img src="{{ asset('new_look/assets/images/lonux-logo-300.png') }}" alt="" height="42">
                <!-- <span class="logo-lg-text-light">H</span> -->
            </span>
        </a>

        <a href="{{ route('home') }}" class="logo logo-light text-center">
            <span class="logo-sm">
                <img src="{{ asset('new_look/assets/images/lonux-logo-300.png') }}" alt="" height="24">
            </span>
            <span class="logo-lg">
                <img src="{{ asset('new_look/assets/images/lonux-logo-300.png') }}" alt="" height="22">
            </span>
        </a>
    </div>

    <div class="h-100" data-simplebar>

        <!-- User box -->
        <div class="user-box text-center">
            <img src="{{ asset('new_look/assets/images/users/user.png') }}" alt="user-img" title="Mat Helme"
                class="rounded-circle avatar-md">
            <div class="dropdown">
                <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block"
                    data-toggle="dropdown">{{ Auth::user()->name }}</a>
                <div class="dropdown-menu user-pro-dropdown">

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user mr-1"></i>
                        <span>My Account</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-settings mr-1"></i>
                        <span>Settings</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-lock mr-1"></i>
                        <span>Lock Screen</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-log-out mr-1"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </div>
            <p class="text-muted">{{ Auth::user()->phone }}</p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul id="side-menu">

                <li class="menu-title">Navigation</li>

                <li>
                    <a href="{{ route('home') }}">
                        <i class="fe-home"></i>
                        <span> Overview </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('users') }}">
                        <i class="fe-user"></i>
                        <span> Users </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('businesses') }}">
                        <i class="fas fa-business-time"></i>
                        <span> Businesses </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('shops') }}">
                        <i class="fas fa-shopping-basket"></i>
                        <span> Shops </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('managers') }}">
                        <i class="fas fa-wrench"></i>
                        <span> Shop Managers </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('items') }}">
                        <i class="fas fa-list"></i>
                        <span> Inventory Items </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('shopItems') }}">
                        <i class="fas fa-shopping-cart"></i>
                        <span> Shop Items </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('shopServices') }}">
                        <i class="fab fa-shopware"></i>
                        <span> Shop Services </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admins.index') }}">
                        <i class="fas fa-user-cog"></i>
                        <span> Super Heroes </span>
                    </a>
                </li>
            </ul>

            <ul id="side-menu">

                <li class="menu-title">SMS Management</li>

                <li>
                    <a href="{{ route('messages.index') }}">
                        <i class="fe-user"></i>
                        <span> Messages </span>
                    </a>
                </li>

                <li>
                    <a href="/bulk_messages">
                        <i class="fe-user"></i>
                        <span> Send Bulk </span>
                    </a>
                </li>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
