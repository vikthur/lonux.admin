@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "Edit Message" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux","subtitle" => "Lists", "item" => "Messages",
    "activeitem" => "Edit Message"])
@endsection


@section('content')
    <div class="row">
    <div class="col-12 text-right">
        <a href="{{route('messages.index')}}" class="btn btn-primary mb-2">View Messages</a>
    </div>
        <div class="card col-8">
            <div class="card-body">
                <h4 class="header-title mb-3 text-center">Create a new SMS message</h4>
                <form class="form" method="post" action="{{route('messages.update', $message->id)}}">
                    @csrf
                    @method('put')
                    <div class="form-group">
                        <label>SMS Title</label>
                        <input name="title" type="text" class="form-control" value="{{$message->title}}" disabled />
                    </div>
                    <div class="form-group">
                        <label>SMS Body</label>
                        <textarea name="message" maxlength="160" type="text" class="form-control" required>{{$message->message}}</textarea>
                    </div>
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">{{$error}}</div>
                        @endforeach
                    @endif
                    @if (Session::has('success'))
                        <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
