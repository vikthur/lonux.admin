@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "Send Bulk Messages" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux","subtitle" => "Lists", "item" => "Messages",
    "activeitem" => "Bulk"])
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12 text-right">
            <a href="{{route('messages.create')}}" class="btn btn-primary mb-2">Create New Message</a>
        </div>

         @if (Session::has('success'))
            <div class="alert alert-success">{{session('success')}}</div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
        @endif

        <form method="post" action="{{route('send_bulk_sms')}}" class="row justify-content-center">
            @csrf
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4>Choose Message</h4>
                        <select name="sms_select" class="form-control mb-3">
                        @foreach ($messages as $message)
                            <option value="{{$message->id}}">{{$message->title}}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4>Choose Recipients</h4>
                        <select name="sms_recipients" class="form-control mb-3">
                            <option value="all_business_users">All business users</option>
                            <option value="business_without_shops">Businesses without shops</option>
                            <option value="shops_without_managers">Shops without managers </option>
                            <option value="shop_managers">Shop managers</option>
                            {{-- <option value="customers">Customers</option> --}}
                            {{-- <option value="dispatch_riders">Dispatch Riders</option> --}}
                    </select>
                    </div>
                </div>
            </div>

            <div class="col-12 text-center">
                <button type="submit" class="btn btn-warning">Send Bulk SMS</button>
            </div>
        </form>
        
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
