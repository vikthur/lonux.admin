@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "SMS Messages" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux","subtitle" => "Lists", "item" => "Messages",
    "activeitem" => "Message List"])
@endsection


@section('content')
    <div class="row">
        <div class="col-12 text-right">
            <a href="{{route('messages.create')}}" class="btn btn-primary mb-2">Create New Message</a>
        </div>
        
        @if (count($messages))
            @foreach ($messages ?? '' as $message)
                <div class="col-sm-6 col-xl-6">
                    <div class="card">
                        <div class="card-body">
                            <h4>{{$message->title}}</h4>
                            <p>{{$message->message}}</p>
                            
                            <a href="{{route('messages.edit', $message->id)}}" class="btn btn-info">Edit</a>
                            <form style="display: inline;" method="post" action="{{route('messages.destroy', $message->id)}}">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-sm-6 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-info">No Messages to deisplay. <a href="{{route('messages.create')}}">create one</a>
                    </div>
                </div>
            </div>
        @endif

        @if (Session::has('done'))
            <div class="alert alert-info">{{session('done')}}</div>
        @endif
        
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
