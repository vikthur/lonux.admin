@extends('layouts.app')

@section('title')
    @include("partials.title-meta", ["title" => "Admin Users" ])
@endsection

@section('topbar')
    @include('partials.topbar', ["title" => "Lonux","subtitle" => "Lists", "item" => "Tables",
    "activeitem" => "Admin Users"])
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="header-title mb-3">lonux inventory items</h4>

                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive nowrap w-100">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Is Active</th>
                                <th>Level</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($heroes as $key => $hero)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $hero->name }}</td>
                                    <td>{{ $hero->phone }}</td>
                                    <td>{{ $hero->email }}</td>
                                    <td>{{ $hero->is_active == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ $hero->level }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->
@endsection

@section('scripts')
@endsection
