@extends('layouts.app')

@section('title')
Add Hero
@endsection

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 justify-content-center">
          <div class="col-sm-6">
            <h1>Create a Super Hero</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- form start -->
              @if (session('success'))
                <div class="alert alert-success mb-2">
                {{session('success')}}
                </div>
              @endif
              @if (session('user'))
                <div class="alert alert-info mt-2 mb-2">
                  <h4>Super Hero Detils</h4>
                  <p>Name: <strong>{{session('user')->name}}</strong></p>
                  <p>Phone: <strong>{{session('user')->phone}}</strong></p>
                  <p>Email: <strong>{{session('user')->email}}</strong></p>
                  <p>Default Password: <strong>password</strong></p>
                </div>
              @endif
              <form method="post" action="/admins">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="adminName">Name</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="adminName" placeholder="Enter Name">
                    @error('name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="adminPhone">Phone</label>
                    <input type="text" name="phone" class="form-control @error('phone') is-invalid @enderror" id="adminPhone" placeholder="Phone">
                    @error('phone')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn lo-btn">Create SuperHero</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
@endsection