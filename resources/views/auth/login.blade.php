<!DOCTYPE html>
<html lang="en">

<head>
    @include("partials.title-meta", ['title' => 'Login'])

    @include('partials.head-css')

</head>

<body class="auth-fluid-pages loading pb-0">

    <div class="auth-fluid" style="background-image: url({{ asset('new_look/assets/images/bg-auth.jpg') }});">
        <!--Auth fluid left content -->
        <div class="auth-fluid-form-box">
            <div>
                <div class="card-body">

                    <!-- Logo -->
                    <div class="auth-brand text-center">
                        <div class="auth-logo">
                            <a href="index.html" class="logo auth-logo-dark">
                                <span class="logo-lg">
                                    <img src="{{ asset('new_look/assets/images/logo-dark.png') }}" alt="" height="26">
                                </span>
                            </a>

                            <a href="index.html" class="logo auth-logo-light">
                                <span class="logo-lg">
                                    <img src="{{ asset('new_look/assets/images/logo-light.png') }}" alt=""
                                        height="26">
                                </span>
                            </a>
                        </div>
                    </div>

                    <!-- title-->
                    <div class="text-center pt-3">
                        <h4 class="mt-0">Sign In</h4>
                        <p class="text-muted mb-4">Enter your phone number and password to access your account.</p>
                        @if (Session::has('unauthorized'))
                            <p class="text-danger">{{ session('unauthorized') }}</p>
                        @endif

                    </div>

                    <!-- form -->
                    <form action="/login" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="telnumber">Phone Number</label>
                            <input name="phone" class="form-control" type="tel" id="telnumber" required
                                placeholder="Enter your phone number">
                        </div>
                        <div class="form-group">
                            <a href="pages-recoverpw.html" class="text-muted float-right"><small>Forgot your
                                    password?</small></a>
                            <label for="password">Password</label>
                            <input name="password" type="password" id="password" class="form-control"
                                placeholder="Enter your password">
                        </div>

                        <div class="form-group mb-0 text-center">
                            <button class="btn btn-primary btn-block" type="submit">Log In </button>
                        </div>

                    </form>
                    <!-- end form-->

                    <!-- Footer-->
                    <footer class="footer footer-alt">
                        <script>
                            document.write(new Date().getFullYear())
                        </script> &copy; Lonux Admin Dashboard
                        </p>
                    </footer>

                </div> <!-- end .card-body -->
            </div>
        </div>
        <!-- end auth-fluid-form-box-->

    </div>
    <!-- end auth-fluid-->

    <!-- Vendor js -->
    <script src="{{ asset('new_look/assets/js/vendor.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('new_look/assets/js/app.min.js') }}"></script>

</body>

</html>
